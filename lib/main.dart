import 'package:flutter/material.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Flutter Demo',
      theme: new ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: new Scaffold(
        body: new MyWidget()
      )
    );
  }
}

class MyWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return new Stack(
      children: <Widget>[
        new Center(
          child: new FlatButton(
            child: new Text("Button"),
            onPressed: () {
              print("Screen size is ${MediaQuery.of(context).size.width}x${MediaQuery.of(context).size.height}");
              print("Screen orientation is ${MediaQuery.of(context).orientation}");
              print("Padding ${MediaQuery.of(context).padding}");
              print("View insets ${MediaQuery.of(context).viewInsets}");
            }
          )
        ),
        new Positioned(
          top: 0.0,
          left: 0.0,
          child: new Container(
            width: 20.0,
            height: 20.0,
            color: Colors.yellow,
          ),
        ),
        new Positioned(
          top: 0.0,
          left: size.width - 20.0,
          child: new Container(
            width: 20.0,
            height: 20.0,
            color: Colors.red,
          ),
        ),
        new Positioned(
          top: size.height - 20.0,
          left: 0.0,
          child: new Container(
            width: 20.0,
            height: 20.0,
            color: Colors.blue,
          ),
        ),
        new Positioned(
          top: size.height - 20.0,
          left: size.width - 20.0,
          child: new Container(
            width: 20.0,
            height: 20.0,
            color: Colors.green,
          ),
        )
      ]
    );
  }
}